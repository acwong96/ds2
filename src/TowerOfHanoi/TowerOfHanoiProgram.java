package TowerOfHanoi;

import java.util.Scanner;
public class TowerOfHanoiProgram {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the amount of disk: ");
        int disk = input.nextInt();
        System.out.println("Movement for disk in Tower of Hanoi : \n");
        long startTime = System.nanoTime();//starts time in nanoseconds
        Towers(disk, 'A', 'C', 'B');
        long stopTime = System.nanoTime();// ends time
        System.out.println("Elasped Time was: " + ((stopTime - startTime)) + " nanoseconds."); //prints time in nanoseconds
    }

    public static void Towers(int disk, char Start, char Dest, char Aux) {
        if (disk == 1) { //if there exist only 1 disk move it over
            System.out.println("Move disk 1 from " + Start + " to " + Dest);
            return;

        }
        Towers(disk - 1, Start, Aux, Dest); //if more than 1 disk shifts top disk over and performs itself
        System.out.println("Move disk " + disk + " from " + Start + " to " + Dest); //shows movement of disk
        Towers(disk - 1, Aux, Dest, Start);
    }
}

